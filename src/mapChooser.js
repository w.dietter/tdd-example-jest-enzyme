const mapChooser = locationName => {
  if (!locationName) {
    locationName = 'berlin';
  }
  return `${locationName}.jpg`;
};

export default mapChooser;
