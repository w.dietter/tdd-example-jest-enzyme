import React from 'react';

import './Map.css';

const Map = ({ imageName }) => {
  return (
    <div className='MapBox'>
      <img src={`images/${imageName ? imageName : 'none'}.png`} alt='default' />
    </div>
  );
};

export default Map;
