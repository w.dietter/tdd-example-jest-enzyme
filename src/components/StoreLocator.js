import React, { useState } from 'react';

import Header from './Header';
import Button from './Button';
import Map from './Map';
import mapChooser from '../mapChooser';

const initialState = [
  {
    location: 'portland',
    address: '123 Portland Dr'
  },
  {
    location: 'astoria',
    address: '123 Astoria Dr'
  },
  {
    location: '',
    address: ''
  }
];

const StoreLocator = () => {
  const [currentMap, setCurrentMap] = useState('none');
  const handleChooseMap = e => {
    setCurrentMap(e.target.value);
  };
  return (
    <>
      <Header />
      {initialState.map((shop, idx) => (
        <Button
          handleClick={handleChooseMap}
          key={idx}
          location={shop.location}
        />
      ))}
      <Map imageName={currentMap} />
    </>
  );
};

export default StoreLocator;
