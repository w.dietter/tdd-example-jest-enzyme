import React from 'react';
import Button from '../Button';
import { shallow } from 'enzyme';

describe('<Button/>', () => {
  let mountedButton;
  beforeEach(() => {
    mountedButton = shallow(<Button />);
  });

  it('renders without crashing', () => {
    expect(mountedButton).not.toBeFalsy();
  });

  it('renders a button', () => {
    const button = mountedButton.find('button');
    expect(button.length).toBe(1);
  });
});

describe('when a location is passed to it', () => {
  let mountedButton;
  let props;

  beforeEach(() => {
    props = {
      location: 'Location 1'
    };
    mountedButton = shallow(<Button {...props} />);
  });

  it('displays the location', () => {
    const locName = mountedButton.find('.location-button');
    expect(locName.text()).toEqual('Location 1');
  });

  it('call a funtion passed to it when clicked', () => {
    const mockCallback = jest.fn();
    const mountedButtonWithCallback = shallow(
      <Button handleClick={mockCallback} />
    );
    mountedButtonWithCallback.find('button').simulate('click');
    expect(mockCallback.mock.calls.length).toBe(1);
  });
});

describe('when no location is passed to it', () => {
  let mountedButton;
  let props;

  beforeEach(() => {
    props = {
      location: undefined
    };
    mountedButton = shallow(<Button {...props} />);
  });

  it('displays the location', () => {
    const locName = mountedButton.find('.location-button');
    expect(locName.text()).toEqual('All locations');
  });
});
