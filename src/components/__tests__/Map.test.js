import React from 'react';
import Map from '../Map';
import { shallow } from 'enzyme';

describe('<Map/>', () => {
  let mountedMap;
  let props;

  beforeEach(() => {
    props = {
      location: undefined,
      imageName: 'test'
    };
    mountedMap = shallow(<Map {...props} />);
  });

  it('renders without crashing', () => {
    expect(mountedMap).not.toBeFalsy();
  });

  it('contains a map image', () => {
    const image = mountedMap.find('img');
    expect(image.length).toBe(1);
  });

  it('displays the map imageName passed to it', () => {
    const testMap = mountedMap.find('img[src="images/test.png"]');
    expect(testMap.length).toBe(1);
  });
});

describe('<Map/> without passed props', () => {
  let mountedMap;

  beforeEach(() => {
    mountedMap = shallow(<Map />);
  });

  it('displays the none map when no params are given', () => {
    const defaultMap = mountedMap.find('img[src="images/none.png"]');
    expect(defaultMap.length).toBe(1);
  });
});
