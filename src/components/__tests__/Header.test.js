import React from 'react';
import Header from '../Header';
import { shallow } from 'enzyme';

describe('<Header/>', () => {
  let mountedHeader;
  beforeEach(() => {
    mountedHeader = shallow(<Header />);
  });

  it('renders without crashing', () => {
    expect(mountedHeader).not.toBeFalsy();
  });

  it('renders a logo', () => {
    const logoImg = mountedHeader.find(
      'img[src="images/wired-brain-logo.png"]'
    );
    expect(logoImg.length).toBe(1);
  });
});
