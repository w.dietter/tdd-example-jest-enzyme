import React from 'react';
import StoreLocator from '../StoreLocator';
import { shallow } from 'enzyme';

let mountedStoreLocator;
beforeEach(() => {
  mountedStoreLocator = shallow(<StoreLocator />);
});

describe('<StoreLocator/>', () => {
  it('renders without crashing', () => {
    expect(mountedStoreLocator).not.toBeFalsy();
  });

  it('renders a header', () => {
    const headers = mountedStoreLocator.find('Header');
    expect(headers.length).toBe(1);
  });

  it('renders two buttons', () => {
    const buttons = mountedStoreLocator.find('Button');
    expect(buttons.length).toBe(3);
  });

  it('renders a map', () => {
    const maps = mountedStoreLocator.find('Map');
    expect(maps.length).toBe(1);
  });
});

describe('chooseMap', () => {
  it('updates the currentMap state using location passed to it', () => {
    let mountedStoreLocator = shallow(<StoreLocator />);
    let mockEvent = { target: { value: 'testland' } };
  });
});
