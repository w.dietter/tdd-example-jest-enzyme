import React from 'react';

import './Button.css';

const Button = ({ location, handleClick }) => {
  return (
    <button value={location} onClick={handleClick} className='location-button'>
      {location ? location : 'All locations'}
    </button>
  );
};

export default Button;
