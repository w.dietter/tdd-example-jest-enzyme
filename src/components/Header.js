import React from 'react';

const Header = () => {
  return (
    <div className='Header'>
      <img src='images/wired-brain-logo.png' alt='logo' width='300' />
    </div>
  );
};

export default Header;
