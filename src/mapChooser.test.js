import mapChooser from './mapChooser';

describe('mapChooser', () => {
  it('returns an image file name based on input given to it', () => {
    let expected = 'portland.jpg';
    let actual = mapChooser('portland');
    expect(actual).toEqual(expected);
  });

  it('returns an default image name when no input is passed', () => {
    let expected = 'berlin.jpg';
    let actual = mapChooser();
    expect(actual).toEqual(expected);
  });
});
